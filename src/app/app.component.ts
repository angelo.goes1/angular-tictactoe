import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'tictactoe';
  gameOn: boolean = true;
  turn: boolean = false;
  gameState: string[] = ['', '', '', '', '', '', '', '', ''];
  message: string = '';
  hardmode: boolean = false;

  ngOnInit(): void {}

  cpuPlay() {
    if (this.gameOn) {
      var counter = 0;
      var index = Math.floor(Math.random() * 99);
      while (this.gameState[index] != '' && counter < 99) {
        index = Math.floor(Math.random() * 9);
        counter++;
      }

      if (counter >= 99) {
        this.gameDraw();
      } else {
        console.log('CPU plays at position ' + index);
        this.gameState[index] = 'O';
      }
      this.turn = !this.turn;
      this.check();
    }
  }

  humanPlay(i: number) {
    if (this.gameOn) {
      if (this.gameState[i] == '') {
        if (!this.turn) {
          console.log('Human plays at position ' + i);
          this.gameState[i] = 'X';
        }
        this.turn = !this.turn;
        this.check();
        this.cpuPlay();
      }
    }
  }

  checkState(state: number[]) {
    if (
      this.gameState[state[0]] == this.gameState[state[1]] &&
      this.gameState[state[1]] == this.gameState[state[2]] &&
      this.gameState[state[0]] != '' &&
      this.gameState[state[1]] != '' &&
      this.gameState[state[2]] != ''
    ) {
      this.message = this.gameState[state[0]] + ' ganhou!';
      this.gameOn = false;
    }
  }

  check() {
    var winStates = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];

    for (let index = 0; index < winStates.length; index++) {
      this.checkState(winStates[index]);
    }
  }

  gameDraw() {
    this.message = 'Velha';
    this.gameOn = false;
  }

  resetGame() {
    this.gameState = ['', '', '', '', '', '', '', '', ''];
    this.gameOn = true;
    this.message = '';
    this.turn = false;
  }
}
